/**
 * jflex file to create scanner for mini Pascal language
 * 
 */

/* Declarations */


%%
/*public class Scanner(){
  hashmap <String, tokenType> lookUpTable = new hashmap();
  lookUpTable.put("function", tokenType.FUNCTION)
}*/
%class  MyScannerTwo   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}


/* Patterns */

other         = .
letter        = [A-Za-z]
word          = {letter}+
whitespace    = [ \r\t\n]
digit		  = [0-9]
number		  = {digit}{digit}*
and			  = and
not			  = not
div			  = div
or			  = or
if			  = if
else		  = else
then		  = then
of			  = of
while		  = while
begin		  = begin
do			  = do
end			  = end
read		  = read
write		  = write
var			  = var
procedure	  = procedure
array		  = array
program		  = program


/* Symbols (only some are recognized)*/
symbol        =(\<|\>|\*|\/|\+|\-|\=|<>|:=|<=|>=|:)
addop		  =(\+|\-)








%%
/* Lexical Rules */

{word}     {
             /** Print out the word that was found. */
             //System.out.println("Found a word: " + yytext());
             return( new Token( yytext()));
            }

{whitespace}  {  /* Ignore Whitespace */

              }

/* What the scanner will do should it find a character that fits into other.*/
{other}    {
             System.out.println("Illegal char: '" + yytext() + "' found.");
           }
		   
/*all of the keywords and symbols lexical rules*/

{number}  {
            return new Token( yytext());
            }
{digit}  {
            return new Token( yytext());
            }
			
{and}  {
            return new Token( yytext());
            }

{not}  {
            return new Token( yytext());
            }

{div}  {
            return new Token( yytext());
            }

{or}  {
            return new Token( yytext());
            }

{if}  {
            return new Token( yytext());
            }

{else}  {
            return new Token( yytext());
            }

{then}  {
            return new Token( yytext());
            }

{of}  {
            return new Token( yytext());
            }

{while}  {
            return new Token( yytext());
            }

{begin}  {
            return new Token( yytext());
            }

{do}  {
            return new Token( yytext());
            }

{end}  {
            return new Token( yytext());
            }

{read}  {
            return new Token( yytext());
            }

{write}  {
            return new Token( yytext());
            }

{var}  {
            return new Token( yytext());
            }

{procedure}  {
            return new Token( yytext());
            }

{array}  {
            return new Token( yytext());
            }

{program}  {
            return new Token( yytext());
            }
			
{symbol}    {
            return new Token( yytext());
            }
			
{addop}    {
            return new Token( yytext());
            }
